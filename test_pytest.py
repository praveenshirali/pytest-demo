# -*- coding: utf-8 -*-
import sys
import pytest

def _dbg(msg):
    """Write a message to stdout"""
    sys.stdout.write("\n    " + msg + "\n")

@pytest.fixture(autouse=True)
def pause_after_each_test(request):
    request.addfinalizer(raw_input)
















































#   A lightning talk on
#
#   ┌─┐┬ ┬┌┬┐┌─┐┌─┐┌┬┐
#   ├─┘└┬┘ │ ├┤ └─┐ │ 
#   ┴   ┴o ┴ └─┘└─┘ ┴
#
#
#   Praveen Shirali
#   RiptideIO (www.riptideio.com)
#
#   PyCon India 2014



















#   pytest is a mature full-featured Python testing tool
#   that helps you write better programs.
#
#
#   [1] As a test runner, it can run your existing tests:
#       * nose, unittest, doctest ...
#
#
#   [2] As a framework, you can write and execute your
#       tests using py.test
#



















#   *** http://pytest.org ***
#
#   Installation:
#       pip install -U pytest
#       easy_install -U pytest
#
#   Quick Test:
#       > py.test --version
#         This is pytest version 2.6.3, imported from ...
#
#   NOTE: py-dot-test on the command line



















# Invoke from CLI
# $> py.test <options> <path-to-tests>

# Or, invoke it from your python code
import pytest

if __name__ == '__main__':
    # pytest.main(["list", "of", "cli", "args"])
    # pytest.main("string-of-cli-args")
    pytest.main(["--verbose", "-s"])




















# Standard test discovery applies:
# -------------------------------
#
#   * Modules starting with `test`
#
#   * Classes starting with `Test`
#     -- classes are optional!
#
#   * Methods starting with `test`
#
#   * It's configurable



















#
#   Fixture
#       
#       Helps you create an environment
#       where your `code-under-test` can
#       run in a controlled manner.
#
#   setUp       <-- Create the environment
#   tearDown    <-- Destroy the environment
#




















# A typical self-contained pytest fixture

@pytest.fixture
def my_fixture(request):

    _dbg("setUp goes here")

    def teardown():
        _dbg("tearDown goes here")

    request.addfinalizer(teardown)
    return ">>> an object <<<"


















# A test which uses the fixture:

# The fixture name is specified in
# the test function args
def test_using_my_fixture(my_fixture):

    # within the test, the `fixture` contains
    # the return value.
    # in this case: ">>> an object <<<"
    #
    _dbg("Fixture returned %s" % my_fixture)



















# Lets define another fixture
@pytest.fixture
def outer_fixture(request):
    _dbg("Outer:setUp")
    def fin():
        _dbg("Outer:tearDown")
    request.addfinalizer(fin)
    return "the outer fixture yo!"

def test_two(outer_fixture, my_fixture):
    _dbg("my_fixture returned %s " % my_fixture)
    _dbg("I am %s" % outer_fixture)


















# Lets add hierarchy
@pytest.fixture
def inner_fixture(request, outer_fixture):
    _dbg("Inner:setUp")
    def fin():
        _dbg("Inner:tearDown")
    request.addfinalizer(fin)
    return "Inner Fixture"

#           [outer-fixture]
#                   \
def test_three(inner_fixture, my_fixture):
    _dbg("Inside test number three")

















#   Fixtures can have scope
#   -----------------------

#   @pytest.fixture(scope="function") <- DEFAULT
#   = @pytest.fixture

#   @pytest.fixture(scope="module")
#   @pytest.fixture(scope="session")
#
#   $> py.test --fixtures




















#   Parametize tests

# The `object` named "value" must take
# "one", "two", "three" on each test iteration.

@pytest.mark.parametrize("value", [
    "one", "two", "three"
])
def test_with_parametrized_values(value):
    _dbg("Test value: %s" % value)




















def test_assert_numbers():
    expected = "ABC"
    assert "ABCD" == expected

def test_assert_bool():
    with pytest.raises(AssertionError):
        assert False is True

def test_assert_lists():
    expected = ["a", "b", "c"]
    assert ["a", "x", "c"] == expected



















# Other features!

#   @pytest.mark.<label>
#       Tag/label your tests and run any
#       diagonal of tests. 
#   @pytest.mark.skipif(<expression>)
#       Skip if a condition is true.
#       .skipif(sys.platform == "win32")






















# *** conftest.py ***
#   Define py.test hooks and common fixtures here
#   They'll be available in multiple modules.

# *** pytest.ini ***
#   Configure your tests from one file

# *** Use TOX (https://testrun.org/tox/)***
#   Test against multiple flavors of python

# --pdb (CLI arg) & pytest.set_trace()
#   Dive into interactive debugging with PDB on
#   failing tests.

















# *** PLUGINS ***
#
#   xdist
#       - Test Driven Development
#         with `looponfail`
#       - Distributed testing
#           - Multiple sub-processes
#           - Multiple machines
#
# And a whole lot more ! ...




















#
#
#   Thank you
#
#
#   Praveen Shirali
#   praveen@riptideio.com
#
#   bitbucket.org/praveenshirali/pytest-demo
#
#









